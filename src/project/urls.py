from django.conf import settings
from django.conf.urls import include, url
from django.contrib import admin

from django.urls import path

from django.views.generic.base import TemplateView


urlpatterns = [
    path('admin/', admin.site.urls),
    url(r'^', include('app.urls')),
    path('accounts/', include('allauth.urls')),
    path('api/', include('api.urls'))
]

if 'localhost' in settings.MEDIA_URL:
    from django.conf.urls.static import static

    # Serve media files from development server
    urlpatterns += static(
        '/media/',
        document_root=settings.MEDIA_ROOT
    )
