# Add any development specific overrides here.
from project.settings import *


# Disable the cache to ensure template/view changes are not cached
# so that changes immediately reflect.
CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.dummy.DummyCache',
    }
}
