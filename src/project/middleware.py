import base64

from django.conf import settings
from django.http import HttpResponse
from django.utils.deprecation import MiddlewareMixin

# This can also easily be done with using nginx
# Basically force basic auth on anything other than a production instance


class BasicAuthMiddleware(MiddlewareMixin):
    """
    Basic auth for sandbox environments.
    """
    def process_request(self, request):
        if not settings.DEBUG:
            return

        if request.META.get('HTTP_HOST') == 'localhost:8000':
            return

        auth = request.META.get('HTTP_AUTHORIZATION', '').split()

        if len(auth) == 2 and auth[0].lower() == 'basic':
            expected = base64.b64encode(
                b'weather:Bt79'
            ).decode()

            if auth[1] == expected:
                return

        response = HttpResponse()
        response.status_code = 401
        response['WWW-Authenticate'] = 'Basic realm="Secured"'

        return response
