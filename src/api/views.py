from django.db.models import Count
from rest_framework import viewsets
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated

from app import models

from . import serializers


class ForcastViewset(viewsets.ModelViewSet):
    serializer_class = serializers.ForcastSerializer
    queryset = models.Forcast.objects.all()
