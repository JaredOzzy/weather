from rest_framework import serializers

from app import models


class ForcastSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = models.Forcast
        fields = '__all__'
