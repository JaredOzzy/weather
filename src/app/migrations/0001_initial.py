# Generated by Django 3.1.8 on 2021-04-12 21:04

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Forcast',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('city', models.CharField(max_length=255)),
                ('date', models.DateTimeField()),
                ('temp_min', models.CharField(max_length=255)),
                ('temp_max', models.CharField(max_length=255)),
                ('realfeel_temp_min', models.CharField(max_length=255)),
                ('realfeel_temp_max', models.CharField(max_length=255)),
                ('sun_rise', models.DateTimeField()),
                ('sun_set', models.DateTimeField()),
                ('moon_rise', models.DateTimeField()),
                ('moon_set', models.DateTimeField()),
                ('moon_phase', models.CharField(max_length=255)),
                ('day_long_phrase', models.CharField(max_length=255)),
                ('day_percipitation_probability', models.CharField(max_length=255)),
                ('day_rain_probability', models.CharField(max_length=255)),
                ('day_thunderstorm_probability', models.CharField(max_length=255)),
                ('day_wind_speed', models.CharField(max_length=255)),
                ('day_wind_direction', models.CharField(max_length=255)),
                ('day_wind_degrees', models.CharField(max_length=255)),
                ('day_rain_total', models.CharField(max_length=255)),
                ('night_long_phrase', models.CharField(max_length=255)),
                ('night_percipitation_probability', models.CharField(max_length=255)),
                ('night_rain_probability', models.CharField(max_length=255)),
                ('night_thunderstorm_probability', models.CharField(max_length=255)),
                ('night_wind_speed', models.CharField(max_length=255)),
                ('night_wind_direction', models.CharField(max_length=255)),
                ('night_wind_degrees', models.CharField(max_length=255)),
                ('night_rain_total', models.CharField(max_length=255)),
            ],
        ),
    ]
