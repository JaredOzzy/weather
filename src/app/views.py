# Create your views here.
from django.views.generic.base import TemplateView

from celery.execute import send_task

from . import models


class HomePageView(TemplateView):

    template_name = "app/home.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context
