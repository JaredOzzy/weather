import requests
import logging

from django.conf import settings
from django.core.management.base import BaseCommand

from app import models

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    '''
    We are only getting and displaying cape town information
    and we already know the key for cape town from the endpoint.
    /locations/v1/cities/search?apikey=nYqqSrP9x7uH6g32zq0wA4KOPOTA3RYH&q=cape%20town
    which is 306633 so we just hard code this, otherwise we can also create
    city objects to relate to the daily forcast and query by city.

    we also only have access to the 5 day forcast as per the free API.

    News24 makes use of accuweather which im using below.
    '''
    city_key = '306633'
    url = f'http://dataservice.accuweather.com/forecasts/v1/daily/5day/306633?metric=true&details=true&apikey={settings.WEATHER_API_KEY}'
    model = models.Forcast
    headers = {'Accept': 'application/json'}

    def handle(self, *args, **options):
        self.get_weather()

    def map_fields(self, data):
        '''
        Here we can do a lot more than just hardcoding the data
        like im currently doing, here we can map the data from out models
        to a mapping relating to what we get from the api and loop through the
        key value pairs and then create a mapping for each forcast.

        but due to time constraints i will just be hardcoding our relations we are looking for
        '''

        updated_data = {
            'city': 'Cape Town',
            'date': data['Date'],
            'temp_max': data['Temperature']['Maximum']['Value'],
            'temp_min': data['Temperature']['Minimum']['Value'],
            'realfeel_temp_max': data['RealFeelTemperature']['Minimum']['Value'],
            'realfeel_temp_min': data['RealFeelTemperature']['Minimum']['Value'],
            'sun_rise': data['Sun']['Rise'],
            'sun_set': data['Sun']['Set'],
            'moon_rise': data['Moon']['Rise'],
            'moon_set': data['Moon']['Set'],
            'moon_phase': data['Moon']['Phase'],
            'day_long_phrase': data['Day']['LongPhrase'],
            'day_percipitation_probability': data['Day']['PrecipitationProbability'],
            'day_rain_probability': data['Day']['RainProbability'],
            'day_thunderstorm_probability': data['Day']['ThunderstormProbability'],
            'day_wind_speed': data['Day']['Wind']['Speed']['Value'],
            'day_wind_direction': data['Day']['Wind']['Direction']['English'],
            'day_wind_degrees': data['Day']['Wind']['Direction']['Degrees'],
            'day_rain_total': data['Day']['Rain']['Value'],
            'night_long_phrase': data['Night']['LongPhrase'],
            'night_percipitation_probability': data['Night']['PrecipitationProbability'],
            'night_rain_probability': data['Night']['RainProbability'],
            'night_thunderstorm_probability': data['Night']['ThunderstormProbability'],
            'night_wind_speed': data['Night']['Wind']['Speed']['Value'],
            'night_wind_direction': data['Night']['Wind']['Direction']['English'],
            'night_wind_degrees': data['Night']['Wind']['Direction']['Degrees'],
            'night_rain_total': data['Night']['Rain']['Value'],
        }
        return updated_data

    def get_weather(self):
        r = requests.get(url=self.url, headers=self.headers).json()

        logger.info(r)
        if 'DailyForecasts' in r:
            for item in r['DailyForecasts']:
                mapped_data = self.map_fields(
                    data=item,
                )
                logger.info(mapped_data)
                updated, created = self.model.objects.update_or_create(
                    **mapped_data
                )
                if created:
                    logger.info(
                        f'Object Created Succesfully: {created}'
                    )
                else:
                    logger.info(
                        f'Object Already Exists, updating object: {updated}'
                    )
                    logger.info(
                        f'Successfully updated object: {updated}'
                    )
            return r
        else:
            logger.error('Something Went Wrong')
