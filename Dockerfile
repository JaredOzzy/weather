FROM python:3.9.2-slim

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

RUN pip install --upgrade pip
COPY requirements.txt /
RUN pip install -r /requirements.txt --no-cache-dir

COPY . /opt/workspace
WORKDIR /opt/workspace/src

RUN cd /opt/workspace/src && \
    ./manage.py collectstatic --noinput

ENTRYPOINT ["/opt/workspace/entrypoint.sh"]
