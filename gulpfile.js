var autoprefixer = require('gulp-autoprefixer');
let cleanCSS = require('gulp-clean-css');
var concat = require('gulp-concat');
var gulp = require('gulp');
var sass = require('gulp-sass');

// To match upstream Bootstrap's level of browser compatibility,
// set Autoprefixer's browsers option to:
var autoprefixerOptions = {
  browsers: [
      "Android 2.3",
      "Android >= 4",
      "Chrome >= 20",
      "Firefox >= 24",
      "Explorer >= 8",
      "iOS >= 6",
      "Opera >= 12",
      "Safari >= 6"
  ]
};

gulp.task('styles', function(){
  return gulp.src('/usr/src/app/static/app/scss/styles.scss')
    .pipe(sass()) // Converts SASS to CSS with gulp-scss
    .pipe(cleanCSS({compatibility: 'ie8'}))
    .pipe(autoprefixer(autoprefixerOptions))
    .pipe(gulp.dest('/usr/src/app/static/app/css/'))
});

gulp.task('scripts', function() {
    return gulp.src([
      '/node_modules/jquery/dist/jquery.slim.min.js',
      '/node_modules/popper.js/dist/umd/popper.min.js',
      '/node_modules/bootstrap/dist/js/bootstrap.min.js',
      '/usr/src/app/static/app/js/*.js'
      ])
        .pipe(concat('scripts.js'))
        .pipe(gulp.dest('/usr/src/app/static/app/js/dist/'));
});

// Register a watch task to listen for file updates
gulp.task('watch', function(){
  gulp.watch('/usr/src/app/static/app/scss/**/*.scss',  gulp.series('styles'));
  gulp.watch('/usr/src/app/static/app/js/*',  gulp.series('scripts'));
});
