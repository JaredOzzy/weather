include .env
mkfile_path := $(abspath $(lastword $(MAKEFILE_LIST)))
current_dir := $(notdir $(patsubst %/,%,$(dir $(mkfile_path))))
db_name := $(subst -,_,$(current_dir))

backup:
	# Make sure the postgres container is running
	docker exec -it ${current_dir}_postgres_1 bash -c 'pg_dump -Fc --username $$POSTGRES_USER $$POSTGRES_USER > /tmp/latest.dump'

restore:
	@echo "Did you run make up? [y/N] " && read ans && [ $${ans:-N} == y ]
	docker exec -it \
	${current_dir}_postgres_1 bash -c \
	'pg_restore --no-owner --no-acl --verbose --username ${db_name} --dbname ${db_name} /tmp/latest.dump'

redo:
	$(MAKE) down
	rm -rf volumes/database
	$(MAKE) latest
	# Now run: make up
	# Then run: make restore

bash:
	docker exec -it ${current_dir}_django_1 /bin/bash

build:
	docker-compose build

collectstatic:
	docker exec -it ${current_dir}_django_1 ./manage.py collectstatic --noinput

copysitepackages:
	docker cp ${current_dir}_django_1:/usr/local/lib/python3.6/site-packages/ site-packages

createsuperuser:
	docker exec -it ${current_dir}_django_1 ./manage.py createsuperuser

super:
	$(MAKE) createsuperuser

down:
	docker-compose down

makemigrations:
	docker-compose run --rm django ./manage.py makemigrations

migrations:
	$(MAKE) makemigrations

makemigrationsmerge:
	docker-compose run --rm django ./manage.py makemigrations --merge

merge:
	$(MAKE) makemigrationsmerge

migrate:
	docker exec -it ${current_dir}_django_1 ./manage.py migrate

m:
	$(MAKE) migrate

shell:
	docker exec -it ${current_dir}_django_1 ./manage.py shell

up:
	docker-compose up

forcast:
	docker-compose run --rm django ./manage.py get_forcast