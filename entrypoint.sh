#!/bin/bash
set -e

function postgres_ready(){
python << END
import sys
import psycopg2
try:
    conn = psycopg2.connect(dbname="$POSTGRES_USER", user="$POSTGRES_USER", password="$POSTGRES_USER", host="postgres")
except psycopg2.OperationalError:
    sys.exit(-1)
sys.exit(0)
END
}

if [ "$POSTGRES_HOST" == "postgres" ] ;then
  echo "Local entrypoint"
  until postgres_ready; do
    >&2 echo "Waiting for postgres"
    sleep 2
  done

  >&2 echo "Postgres is up \o/"
fi

# only apply when remotely deployed
if [ "$POSTGRES_HOST" != "postgres" ] ;then
  python manage.py migrate --noinput
fi

exec "$@"
